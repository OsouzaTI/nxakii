import { Button, Divider, Input, Select, Text, Textarea, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useState } from "react";
import { getAllGroups } from "../../api/group";
import { getNoteById } from "../../api/note/[id]";

export default function Edit(props) {
    
    const { note, groups } = props;
    
    const [group, setGroup] = useState(note.group_id);    
    const [title, setTitle] = useState(note.title);
    const [text, setText] = useState(note.body);

    const router = useRouter();
    
    async function editNote() {
        const { id } = router.query;        
        const list = {title, body: text, groupId: group};
        await fetch(`/api/note/edit/${id}`, {
            method: 'POST',
            body: JSON.stringify(list),
            headers: {'Content-Type': 'application/json'}
        });
        router.push('/note')
    }

    return (
        <VStack h={'full'} justifyContent={'center'}>
            <VStack gap={4}>
                <VStack w={['xs', 'sm', 'md', 'lg']} alignItems={'center'} gap={2}>
                    <VStack w={'full'} gap={4}>
                        <Text fontSize={20}>Edit a note</Text>
                        <Divider />
                        <Select value={group} onChange={({target})=>setGroup(target.value)}>
                            {groups.map(({id, title}, i)=><option key={i} value={id}>{title}</option>)}
                        </Select>
                        <Divider />
                        <Text fontSize={20}>Title</Text>
                        <Input value={title} onChange={({target})=>setTitle(target.value)} />
                        <Text fontSize={20}>Text</Text>
                        <Textarea value={text} maxH={'lg'} resize={true} onChange={({target})=>setText(target.value)} />
                    </VStack>
                </VStack>
                <Button w={'full'} color={'#222'} variant={'solid'} colorScheme={'whiteAlpha'} onClick={editNote}>Edit</Button>
            </VStack>
        </VStack>

    );

}

export async function getServerSideProps(context) {

    
    const { id } = context.params;
    const note = await getNoteById(id);
    const groups = await getAllGroups();

    console.log(note);
    return {
        props: {note, groups},
    }

}
  
