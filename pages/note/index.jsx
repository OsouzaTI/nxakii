import { SimpleGrid, useBoolean, VStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import SimpleCard from "../../components/Cards/SimpleCard";
import CardContext from "../../components/Contexts/CardContext";
import Filters from "../../components/Filters";
import { fetchAllGroups, fetchAllNotes, fetchNotesByGroup, fetchNotesByTitle } from "../../utils/helpers";
import { isMobileScreen } from "../../utils/responsivity";

export default function index({groups, notes}) {

    const [cardNotes, setCardNotes] = useState(notes);
    const [refresh, setRefresh] = useBoolean();

    // filtro de titulo
    const [filter, setFilterTitle] = useState('');
    // filtro de grupo
    const [group, setFilterGroup] = useState(-1);

    useEffect(()=>{

        async function refreshCards() {
            let notes = await fetchAllNotes();                
            setCardNotes(notes);
        }
        
        refreshCards();
        
    }, [refresh]);
    
    useEffect(()=>{

        async function refreshCards() {
            if(filter.length > 0 && (filter.length % 4) == 0)  {
                let notes = await fetchNotesByTitle(filter);
                setCardNotes(notes);
            }
        }

        refreshCards();

    }, [filter])

    useEffect(()=>{

        async function refreshCards() {
            if(group > -1)  {
                let notes = await fetchNotesByGroup(group);
                setCardNotes(notes);
            }
        }

        refreshCards();

    }, [group])

    return (
        <CardContext.Provider value={{setRefresh, setFilterTitle, setFilterGroup}}>
            <VStack maxH={isMobileScreen() ? 'full' : '100vh'} overflowY={'auto'} p={4}>
                <Filters groups={groups} />
                <SimpleGrid w={'full'} columns={[1, 2, 3, 4]} gap={4}>
                    {cardNotes.map((note, i)=><SimpleCard key={i} {...note} />)}
                </SimpleGrid>
            </VStack>
        </CardContext.Provider>
    );

}

export async function getServerSideProps(context) {

    const groups = await fetchAllGroups();
    const notes = await fetchAllNotes();

    return {
        props: {groups, notes},
    }
    
}