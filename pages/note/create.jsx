import { Button, Divider, Input, Select, Text, Textarea, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react";
import GroupContext from "../../components/Contexts/GroupContext";
import { fetchAllGroups } from "../../utils/helpers";

export default function createList({groups = []}) {

    const [group, setGroup] = useState(groups[0].id ?? -1);    
    const [title, setTitle] = useState();
    const [text, setText] = useState();
    const router = useRouter();



    async function addNote() {
        const list = {date: new Date(), title, body: text, group_id: group};
        
        await fetch('/api/note/add', {
            method: 'POST',
            body: JSON.stringify(list),
            headers: {'Content-Type': 'application/json'}
        });

        router.push('/note')
    }

    useEffect(()=>{

        fetchAllGroups();

    }, [])

    return (
        <VStack h={'full'} justifyContent={'center'}>
            <VStack gap={4}>
                <VStack w={['xs', 'sm', 'md', 'lg']} alignItems={'center'} gap={2}>
                    <VStack w={'full'} gap={4}>
                        <Text fontSize={20}>Create a note</Text>
                        <Divider />
                        <Select value={group} onChange={({target})=>setGroup(target.value)}>
                            {groups.map(({id, title}, i)=><option key={i} value={id}>{title}</option>)}
                        </Select>
                        <Divider />
                        <Text fontSize={20}>Title</Text>
                        <Input value={title} onChange={({target})=>setTitle(target.value)} />                        
                        <Text fontSize={20}>Text</Text>
                        <Textarea value={text} maxH={'lg'} resize={true} onChange={({target})=>setText(target.value)} />
                    </VStack>
                </VStack>
                <Button w={'full'} color={'#222'} variant={'solid'} colorScheme={'whiteAlpha'} onClick={addNote}>Create</Button>
            </VStack>
        </VStack>

    );

}


export async function getServerSideProps(context) {

    const groups = await fetchAllGroups();

    return {
        props: {groups},
    }

}