import { ChakraProvider, useBoolean } from '@chakra-ui/react'
import { useState } from 'react';
import GridLayout from '../components/Layout/GridLayout';
import '../styles/globals.css'

function MyApp({ Component, pageProps }) {

  return (
    <ChakraProvider>
      <GridLayout>        
          <Component {...pageProps} />                  
      </GridLayout>
    </ChakraProvider>
  );  
  
}

export default MyApp
