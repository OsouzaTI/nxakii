import { Button, Divider, Input, Text, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useState } from "react";

export default function CreateGroup() {

    const [title, setTitle] = useState();
    const router = useRouter();

    async function addGroup() {
        const list = {title};
        
        await fetch('/api/group/add', {
            method: 'POST',
            body: JSON.stringify(list),
            headers: {'Content-Type': 'application/json'}
        });

        router.push('/note');

    }

    return (
        <VStack h={'full'} justifyContent={'center'}>
            <VStack gap={4}>
                <VStack w={['xs', 'sm', 'md', 'lg']} alignItems={'center'} gap={2}>
                    <VStack w={'full'} gap={4}>
                        <Text fontSize={20}>Create a group</Text>
                        <Divider />
                        <Text fontSize={20}>Title</Text>
                        <Input onChange={({target})=>setTitle(target.value)} />                        
                    </VStack>
                </VStack>
                <Button w={'full'} color={'#222'} variant={'solid'} colorScheme={'whiteAlpha'} onClick={addGroup}>Create</Button>
            </VStack>
        </VStack>

    );

}
