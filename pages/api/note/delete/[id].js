import supabase from "../../../../utils/supabase";

export default async function id(req, res) {

    const { id } = req.query;
    const {error} = await supabase.from('notes').delete().eq('id', id);

    if(error) {
        res.status(500).send(error);
    } else {
        res.status(201).send({'deleted': 1});
    }

}
