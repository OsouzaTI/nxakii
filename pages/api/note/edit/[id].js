import { dbNotes } from "../../../../utils/connection";

export default async function id(req, res) {

    return await new Promise(function(resolve, reject){

        const { id } = req.query;
        const { title, body, groupId } = req.body;

        const db = dbNotes();    
        db.run("UPDATE notes SET title=?, body=?, group_id=? WHERE id=?;", [title, body, groupId, id], (err) => {        
            if(err) {
                res.send({status: err.message});
            } else {
                res.send({status: 1});
            }
        }, function(err){
            if(err) {
                reject(err.message);
            } else {
                resolve(1);
            }

        });

    }).then(data => res.send(data));

}
