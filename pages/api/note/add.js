import supabase from "../../../utils/supabase";

export default async function add(req, res) {

    let {error} = await supabase.from('notes').insert(req.body);
    console.log(error);
    if(error) {
        res.status(500).send(error);
    } else {
        res.status(201).send({'success': 1});
    }

}

