import supabase from "../../../../utils/supabase";



export default async function(req, res) {
    
    const { id } = req.query;
    let {data, error} = await supabase.from('notes').select().eq('group_id', id);
    if(error) {
        res.status(500).send(error);
    } else {
        res.status(200).json(data);
    }

}
