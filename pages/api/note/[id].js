import supabase from "../../../utils/supabase";

export default async function id(req, res) {

    const { id } = req.query;
    const {data, error} = await supabase.from('notes').select().eq('id', id);
    if(error) {
        res.status(500).send(error);
    } else {
        res.status(200).json(data);
    }

}
