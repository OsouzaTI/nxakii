
import supabase from "../../../utils/supabase";


export default async function title(req, res) {
    
    const { title } = req.body;
    let {data, error} = await supabase.from('notes').select().ilike('title', `%${title}%`);
    if(error) {
        res.status(500).send(error);
    } else {
        res.status(200).json(data);
    }

}
