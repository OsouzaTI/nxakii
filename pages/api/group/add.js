import supabase from "../../../utils/supabase";

export default async function add(req, res) {

    const { title } = req.body;

    let {error} = await supabase.from('groups').insert(req.body);

    if(error) {
        res.status(500).send(error);
    } else {    
        res.status(201).send({'success': 1});
    }

}
