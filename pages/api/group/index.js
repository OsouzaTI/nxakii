import supabase from "../../../utils/supabase"

export default async function index(req, res) {

    let {data, error} = await supabase.from('groups').select('*');
    if(error) {
        res.status(500).send(error);
    } else {
        res.status(200).json(data);
    }

}