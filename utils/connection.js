import sqlite3 from 'sqlite3';

function dbPath(database) {
    const dataBasePath = process.env.DATABASE_PATH ?? '';
    return `${dataBasePath}/${database}.db`;
}

function dbNotes() {
    
    const db = new sqlite3.Database(dbPath('notes'));

    db.serialize(()=>{

        db.run(`
            CREATE TABLE IF NOT EXISTS notes (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                title VARCHAR(100),
                body TEXT,
                date TIMESTAMP,
                url_image TEXT,
                group_id INTEGER NOT NULL,
                FOREIGN KEY(group_id) REFERENCES groups(id)
            );
        `)

    });

    return db;

}

function dbGroups() {

    const db = new sqlite3.Database(dbPath('groups'));

    db.serialize(()=>{

        db.run(`
            CREATE TABLE IF NOT EXISTS groups (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                title VARCHAR(100)
            );
        `)

    });

    return db;

}


export {
    dbNotes,
    dbGroups
}
