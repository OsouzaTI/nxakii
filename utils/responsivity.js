import { useMediaQuery } from "@chakra-ui/react";

function isMobileScreen() {
    const [isMobileScreen] = useMediaQuery('(max-width: 30em)');
    return isMobileScreen;
}

export {
    isMobileScreen
}