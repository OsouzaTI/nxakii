const endpoint = 'http://localhost:3000';

async function fetchAllGroups() {
    console.log('Atualizando lista de grupos...');
    const response = await fetch(`${endpoint}/api/group`);
    const data = await response.json();
    return data;
}

async function fetchAllNotes() {
    const response = await fetch(`${endpoint}/api/note`);
    const data = await response.json();
    return data;
}

async function fetchNotesByTitle(title) {
    const response = await fetch(`${endpoint}/api/note/title`, {
        method: 'POST',
        body: JSON.stringify({title}),
        headers: {'Content-Type': 'application/json'}
    });
    const data = await response.json();
    return data;
}

async function fetchNotesByGroup(groupId) {
    const response = groupId != 0 
        ? await fetch(`${endpoint}/api/note/group/${groupId}`)
        : await fetch(`${endpoint}/api/note`);
    const data = await response.json();
    return data;
}

export {
    fetchAllGroups,
    fetchAllNotes,
    fetchNotesByTitle,
    fetchNotesByGroup
};