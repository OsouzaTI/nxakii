import { Box, HStack, Input, Select } from "@chakra-ui/react";
import { useContext, useEffect, useState } from "react";
import CardContext from "./Contexts/CardContext";

export default function Filters({groups}) {

    const { setFilterTitle, setFilterGroup } = useContext(CardContext);

    const [group, setGroup] = useState(1);
    const [search, setSearch] = useState('');

    async function onChange(value) {
        setGroup(value);
        setFilterGroup(value);
    }

    function onSearch(value) {  
        console.log(value);
        if( (search.length % 4) == 0 ) fetchNotesByTitle(search);     
        setSearch(value);
    }

    return (
        <HStack justifyContent={'end'} w={'full'}>
            <Box>
                <Input value={search} onChange={({target}) => onSearch(target.value)} />
            </Box>
            <Box>
                <Select value={group} onChange={({target})=>onChange(target.value)}>
                    <option value={0}>Todos</option>
                    {groups.map(({id, title}, i)=><option key={i} value={id}>{title}</option>)}
                </Select>
            </Box>
        </HStack>
    );

}
