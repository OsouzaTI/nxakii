import { AddIcon, ArrowForwardIcon, CalendarIcon, HamburgerIcon, SmallAddIcon } from "@chakra-ui/icons";
import { Box, Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerFooter, DrawerHeader, DrawerOverlay, HStack, IconButton, Text, useDisclosure, useMediaQuery, VStack } from "@chakra-ui/react";
import Link from "next/link";
import { useEffect, useRef } from "react";

function NormalSideMenu({menu}) {
    return (
        <VStack mt={4} gap={4}>
            {menu.map(({name, route, icon}, i)=>
                <Link key={i} href={route}>
                    <HStack w={'80%'} justifyContent={'space-between'} _hover={{bg:'#333', cursor: 'pointer'}} p={4}>
                        <Text>{name}</Text>
                        {icon}
                    </HStack>
                </Link>
            )}
        </VStack>
    );
}

function DrawerSideMenu({menu}) {

    const { isOpen, onOpen, onClose } = useDisclosure();
    const btnRef = useRef();

    return (
        <HStack justifyContent={'end'} p={2} >
            <IconButton bg={'#'} _hover={{bg: 'green.500'}} onClick={onOpen} ref={btnRef} icon={<HamburgerIcon />}/>
            <Drawer
                isOpen={isOpen}
                placement={'right'}
                onClose={onClose}
                finalFocusRef={btnRef}
            >
                <DrawerOverlay />
                <DrawerContent bg={'#222222'} color={'#fff'}>
                    <DrawerCloseButton />
                    <DrawerHeader>Menu</DrawerHeader>
                    <DrawerBody>
                        {menu.map(({name, route, icon}, i)=>
                            <Link key={i} href={route}>
                                <HStack onClick={onClose} justifyContent={'space-between'} _hover={{bg:'#333', cursor: 'pointer'}} p={4}>
                                    <Text>{name}</Text>
                                    {icon}
                                </HStack>
                            </Link>
                        )}
                    </DrawerBody>
                    <DrawerFooter></DrawerFooter>
                </DrawerContent>
            </Drawer>
        </HStack>
    );

}

export default function Side() {

    const menu = [
        {name: 'Nova nota', route: '/note/create',  icon: <SmallAddIcon/>},
        {name: 'Notas', route: '/note',  icon: <ArrowForwardIcon/>},
        {name: 'Criar grupo', route: '/group/create',  icon: <CalendarIcon/>},        
    ]

    const [isMobileScreen] = useMediaQuery('(max-width: 30em)');

    return isMobileScreen ? <DrawerSideMenu menu={menu} /> : <NormalSideMenu menu={menu} />;

}
