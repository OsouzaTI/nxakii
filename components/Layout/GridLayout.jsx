import { Grid, GridItem } from "@chakra-ui/react";
import { isMobileScreen } from "../../utils/responsivity";
import Side from "../Side";

export default function GridLayout({children}) {

    function responsive() {

        const normal = {
            templateAreas: ` "side app" `,
            gridTemplateRows: '1fr',
            gridTemplateColumns: '.2fr 1fr',
            h: '100vh'
        };

        const mobile = {
            templateAreas: `
                "side"
                "app"
            `,
            gridTemplateRows: '.01fr 1fr',
            gridTemplateColumns: '1fr',
            w: '100vw',
            h: '100vh'
        };

        return isMobileScreen() ? mobile : normal;

    }

    const gridResponsiveData = responsive();

    return (
        <Grid {...gridResponsiveData} color={'#fff'}>
            <GridItem bg={'#222'} area={'side'}>
                <Side />
            </GridItem>
            <GridItem bg={'#333'} area={'app'}>{children}</GridItem>
        </Grid>
    );

}