import { DeleteIcon, EditIcon } from "@chakra-ui/icons";
import { Box, Flex, HStack, IconButton, Image, Stack, Text, useBoolean, VStack } from "@chakra-ui/react";
import Link from "next/link";
import { useContext } from "react";
import CardContext from "../Contexts/CardContext";


export default function SimpleCard({id, title, body, url_image, date}) {
    
    const {setRefresh} = useContext(CardContext);
    const [loading, setLoading] = useBoolean();

    async function deleteNote() {
        
        setLoading.toggle();        
        fetch(`/api/note/delete/${id}`)
        .then(response => {
            if(response.status == 200) {
                setLoadingCards.toggle()
            } 
            setLoading.toggle();
            setRefresh.toggle();
        })
    }

    async function editNote() {
        
    }

    return (
        <VStack h={"80"} w={'full'} bg={'#454545'} p={2}>
            <HStack w={'full'} justifyContent={'space-between'}>
                <VStack justifyContent={'center'} bg={'#333'} rounded={'full'} w={7} h={7}><Text textAlign={'center'}>{id}</Text></VStack>
                <Flex gap={2}>
                    <Link href={`note/edit/${id}`}><IconButton  bg={'#'} _hover={{bg: 'green.500'}} icon={<EditIcon />} /></Link>
                    {loading 
                        ? <IconButton isLoading bg={'#'} _hover={{bg: 'red.500'}} icon={<DeleteIcon />} />
                        : <IconButton onClick={deleteNote} bg={'#'} _hover={{bg: 'red.500'}} icon={<DeleteIcon />} />
                    }
                </Flex>
            </HStack>
            <VStack w={'full'} gap={4} wordBreak={'break-word'} overflowY={'auto'}>
                {url_image ? <Image alt={'imagem'} src={url_image} width={"64"} height={"48"}/> : <></>}
                <Text mt={10} textAlign={'center'}>{title}</Text>
                <sub>{new Date(date).toLocaleDateString("pt-BR")}</sub>
                <Text h={'full'}>{body}</Text>
            </VStack>
        </VStack>
    )

}
